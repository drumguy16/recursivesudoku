// Globals
let board;
const boardForm = $('#board');
const boardParams = $('#set-board');
const boardParamsSize = $('#board-size');
const boardParamsButton = $('#create-board');
// ---------------------------
//Interactive functionality
// ---------------------------
var demoBoard = [
	'','','',
	'','',6,
	3,'','',

	'',7,'',
	3,'',8,
	'',9,'',

	'','',5,
	'',9,'',	
	4,8,7,

	7,'','',
	'',6,'',
	4,3,'',

	'',6,1,
	'','','',
	2,'',5,

	8,2,'',
	'',5,'',
	6,'','',

	'','',7,
	9,2,5,
	'',8,'',

	9,'','',
	8,'','',
	'',5,7,

	'','','',
	7,'','',
	'','',2
];
var demoBoard2 = [
	2,'',7,
	'',4,'',
	8,'','',
	
	'','','',
	'','',6,
	'','','',

	1,'','',
	'',5,'',
	9,'',7,

	'',1,6,
	3,'','',
	'',5,'',

	2,'','',
	'',1,'',
	7,'',4,

	'','','',
	'','',2,
	'','','',
	
	'','','',
	6,'','',
	'',7,'',

	6,'','',
	'','','',
	'','',9,

	5,'','',
	'','',8,
	'','',''
];
var demoBoard3 = [
	7,3,'',
	'','','',
	8,'',5,

	'',9,'',
	'','','',
	4,'','',

	'','','',
	'','','',
	'',1,7,

	'','','',
	'','',9,
	'','','',

	'',2,9,
	'','','',
	'',5,4,

	'',5,6,
	'','','',
	8,7,'',

	'',2,'',
	'',5,'',
	'',4,'',

	'','','',
	3,'','',
	5,7,'',

	'','','',
	9,6,'',
	'','',''
]

	var demoChars = [1,2,3,4,5,6,7,8,9]
// create board object
boardParamsButton.on('click', function(e) {
	e.preventDefault();
	var boardFormSubmit = document.createElement('button');
	$(boardFormSubmit).text('Solve Board');
	$(boardFormSubmit).attr('id', 'submit-board');
	$(boardFormSubmit).on('click', function(event) {
		event.preventDefault();
		handleBoardSubmit();
	});
	boardForm.empty();
	board = new Board(boardParamsSize.val());
	//Populate board form
	board.createCellRows();
	board.createCellCols();
	board.createBoxRows();
	board.createBoxes();
	displayCharsInputs(board.size);
	$(boardForm).append(boardFormSubmit)
	console.log(board);
});
function handleBoardSubmit() {
	var vals = getCellInputVals();
	var validChars = $('.char-input');
	board.setCellVals(vals);
	setValidChars(validChars);
	board.recursiveSolve();
}
function getCellInputVals() {
	var cells = $('.cell');
	var cellVals = [];
	$.each(cells, function(index, cell) {
		var cellVal = $(cell).val();
		cellVals.push(cellVal);
	})
	return cellVals;
}
function setValidChars($charInputs) {
	var validChars = [];
	$.each($charInputs, function(index, charInput) {
		validChars.push($(charInput).val());
	})
	board.validChars = validChars;
}
function displayCharsInputs(num) {
	var charFields = document.createElement('fieldset');
	$(charFields).attr('id', 'char-fields');
	boardForm.append(charFields);
	for(var i = 0; i < num; i++) {
		var charInput = document.createElement('input');
		$(charInput).addClass('char-input');
		$(charFields).append(charInput);
 	}
}

//-----------------------
// Board Class
// -----------------------
class Board {
	constructor(size) {
		var sqRoot = Math.sqrt(size);
		this.isValidBoard = true;
		this.isSolved = false;
		this.size = size;
		this.validChars;
		this.numOfCells = size * size;
		this.boxRows = {};
		this.cellRows = [];
		this.cellCols = []; 
		if(sqRoot % 1 === 0) {
			this.boxWidth = sqRoot;
			this.boxHeight = sqRoot;
		} else {
			var divisor = this.checkDivisibility(sqRoot);
			if(!divisor) {
				this.isValidBoard = false;
			}
			else if(divisor > this.size/divisor) {
				this.boxWidth = divisor;
				this.boxHeight = this.size/divisor; 
			} else {
				this.boxWidth = this.size/divisor
				this.boxHeight = divisor;
			}
		}
		this.gameState = {
			"boardIterations" : 0,
		}
	}
	checkDivisibility(sqRoot) {
		var divisor = Math.floor(sqRoot);
		while(this.size % divisor != 0 && divisor < (this.size - 1)) {
			divisor += 1;
		}
		if((this.size/divisor) % 1 === 0) {
			return divisor;
		} else {
			return false;
		}
	}
	createCellRows() {
		for(var i = 0; i < this.size; i++) {
			this.cellRows[i] = [];
		} 
	}
	populateCellRowArray(i, val) {
		this.cellRows[i].push(val);
	}
	createCellCols() {
		for(var i = 0; i < this.size; i++) {
			this.cellCols[i] = [];
		} 
	}
	createBoxRows() {
		for(var i = 0; i < this.boxWidth; i++) {
			this.boxRows[i] = [];
			this.appendBoxRow(i);
		}
	}
	createBoxes() {
		if(this.isValidBoard) {
			for(var i = 0;i < this.size; i++) {
				var boxNum = i;
				var box = new Box(boxNum, this.boxWidth, this.boxHeight);
				if(this.boxRows[box.boxRow].length === 0){
					box.boxIndex = 0;
				} else {
					box.boxIndex = this.boxRows[box.boxRow].length;	
				}
				this.boxRows[box.boxRow].push(box);
				this.appendBox(box.boxRow, boxNum, box);
			}
		}
	}
	appendBoxRow(boxRow) {
		var row = document.createElement('div');
		$(row).addClass('box-row');
		$(row).attr('id', 'boxRow-' + boxRow)
		boardForm.append(row);
	}
	appendBox(boxRow, boxNum, boxModel) {
		var boxRow = $('#boxRow-' + boxRow);
		var box = document.createElement('div');
		$(box).addClass('box');
		$(box).attr('id', 'box-' + boxNum);
		boxRow.append(box);
		boxModel.createRows();
		boxModel.createCells();
	}
	setCellVals(vals) {
		for(var boxRow in this.boxRows) {
			for(var box in this.boxRows[boxRow]) {
				for(var row in this.boxRows[boxRow][box].rows) {
					for(var cell in this.boxRows[boxRow][box].rows[row]) {
						var currentCell = this.boxRows[boxRow][box].rows[row][cell]
						var val = vals.shift();
						currentCell.value = val;
						this.cellRows[currentCell.cellRow].push(val);
						this.cellCols[currentCell.cellCol].push(val);
						this.boxRows[boxRow][box].cellChars.push(val);
					}
				}
			}
		}
	}
	findFirstVacant() {
		for(var boxRow in this.boxRows) {
			for(var box in this.boxRows[boxRow]) {
				for(var row in this.boxRows[boxRow][box].rows) {
					for(var cell in this.boxRows[boxRow][box].rows[row]) {
						var currentCell = this.boxRows[boxRow][box].rows[row][cell]
						if(currentCell.value === "") {
							return currentCell;
						}
					}
				}
			}
		}
	}
	recursiveSolve() {
		var firstVacant	= this.findFirstVacant();
		if(this.checkSolved()) {
			console.log('fuck yes!!!!')
			alert('fuck yeahhhhhh!' + ' board iterations: ' + this.gameState.boardIterations);
			return true;
		} else {
			firstVacant.candidates = [];
			for(var i = 0; i < this.validChars.length; i++) {
				firstVacant.setCandidates(this.validChars[i]);
			}
			var candidates = firstVacant.candidates;
			for(var i = 0; i < candidates.length; i++) {
				var val = candidates[i];
				if(candidates.length > 0 && !(firstVacant.checkAll(val))) {
					this.cellRows[firstVacant.cellRow][this.cellRows[firstVacant.cellRow].indexOf(firstVacant.value)] = "";
					this.cellCols[firstVacant.cellCol][this.cellCols[firstVacant.cellCol].indexOf(firstVacant.value)]= "";
					this.boxRows[firstVacant.boxRow][firstVacant.boxIndex].cellChars[this.boxRows[firstVacant.boxRow][firstVacant.boxIndex].cellChars.indexOf(firstVacant.value)]= "";
					firstVacant.value = val;
					this.cellRows[firstVacant.cellRow].push(firstVacant.value);
					this.cellCols[firstVacant.cellCol].push(firstVacant.value);
					this.boxRows[firstVacant.boxRow][firstVacant.boxIndex].cellChars.push(firstVacant.value);
					firstVacant.setCellUI();
					console.log(firstVacant);
					console.log(firstVacant.value)
					if(this.recursiveSolve() === true) {
						return true
					}
				}
				// Backtrack rollback guess and check
				this.gameState.boardIterations++;
				this.cellRows[firstVacant.cellRow][this.cellRows[firstVacant.cellRow].indexOf(firstVacant.value)] = "";
				this.cellCols[firstVacant.cellCol][this.cellCols[firstVacant.cellCol].indexOf(firstVacant.value)]= "";
				this.boxRows[firstVacant.boxRow][firstVacant.boxIndex].cellChars[this.boxRows[firstVacant.boxRow][firstVacant.boxIndex].cellChars.indexOf(firstVacant.value)]= "";
				firstVacant.value = "";
				firstVacant.setCellUI();
			}		
			return false
		}
	}
	checkSolved() {
		for(var boxRow in this.boxRows) {
			for(var box in this.boxRows[boxRow]) {
				for(var row in this.boxRows[boxRow][box].rows) {
					for(var cell in this.boxRows[boxRow][box].rows[row]) {
						var currentCell = this.boxRows[boxRow][box].rows[row][cell]
						if(currentCell.value === "") {
							return false;
						} 
					}
				}
			}
		}
		return true;
	}
}
// --------------------
// Box Class
// --------------------
class Box {
	constructor(boxNum, width, height) {
		this.boxNumber = boxNum;
		this.numOfCells = width * height;
		this.boxWidth = width;
		this.boxHeight = height;
		this.boxRow = Math.floor(boxNum/height);
		this.rows = {};
		this.cellChars = [];
	}
	createRows() {
		for(var i = 0; i < this.boxHeight; i++) {
			this.rows[i] = [];
			this.appendRow(i);
		}
	}
	createCells() {
		for(var i = 0; i < this.numOfCells; i++) {
			var row = Math.floor(i/this.boxWidth);
			var firstCol = (this.boxWidth * this.boxNumber) - (this.boxRow * (this.boxWidth * this.boxHeight));
			var cellRow = (row + this.boxRow * this.boxHeight); 
			// boxWidth * boxNum = startrow
			var col = firstCol + this.rows[row].length;
			var cellNum = i + (this.boxNumber * (this.boxWidth * this.boxHeight));
			var cell = new Cell(cellNum, cellRow, col, this.boxRow, this.boxNumber, this.boxIndex);
			this.rows[row].push(cell);
			this.appendCell(cell.cellNum, row);
		}
	}
	appendRow(rowNum) {
		var boxId = 'box-' + this.boxNumber;
		var box = $('#' + boxId);
		var row = document.createElement('fieldset');
		$(row).addClass('cell-row');
		$(row).attr('id', boxId + 'row-' + rowNum);
		box.append(row);
	}
	appendCell(cellNum, rowNum) {
		var row = $('#box-' + this.boxNumber + 'row-' + rowNum);
		var cell = document.createElement('input');
		$(cell).addClass('cell');
		$(cell).attr('id', cellNum);
		$(cell).val(demoBoard.shift());
		row.append(cell);
	}
}
// ------------------
// Cell Class
// ------------------
class Cell {
	constructor(cellNum, row, col, boxRow, boxNum, boxIndex) {
		this.cellNum = cellNum;
		this.value;
		this.cellRow = row;
		this.cellCol = col;
		this.boxRow = boxRow;
		this.boxNum = boxNum;
		this.boxIndex = boxIndex;
		this.candidates = [];
	}
	checkRow(val) {
		return board.cellRows[this.cellRow].indexOf(val) != -1 ? true : false;
	}
	checkCol(val) {
		return board.cellCols[this.cellCol].indexOf(val) != -1 ? true : false;
	}
	checkBox(val) {
		return board.boxRows[this.boxRow][this.boxIndex].cellChars.indexOf(val) != -1 ? true : false;
	}
	checkAll(val) {
		return this.checkRow(val) || this.checkCol(val) || this.checkBox(val);
	}
	setCandidates(val) {
		if(!(this.checkAll(val))) {
			this.candidates.push(val);
		}
	}
	setCellUI() {
		$(boardForm).find('#' + this.cellNum).val(this.value)
	}
}
// var test = new Board(13);
// console.log("width: " + test.boxWidth + " Height: " + test.boxHeight)
// Stored boards